//
//  ViewController.swift
//  GR57L9
//
//  Created by student on 10/24/17.
//  Copyright © 2017 Web Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var array = "Created by student on 10/24/17.".components(separatedBy: " ")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.dataSource = self
        
    }
    
    //MARK - TableViewDataSource
    //tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: <#T##IndexPath#>)
        cell.textLabel?.text = array[indexPath.row]
        return cell
    }
}

